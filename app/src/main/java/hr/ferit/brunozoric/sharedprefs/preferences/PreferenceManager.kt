package hr.ferit.brunozoric.sharedprefs.preferences

import android.content.Context
import hr.ferit.brunozoric.sharedprefs.MyApplication


class PreferenceManager {

    companion object {
        const val PREFS_FILE = "MyPreferences"
        const val PREFS_KEY_USERNAME = "Bruno"
    }

    fun saveUsername(username: String) {
        val sharedPreferences = MyApplication.ApplicationContext.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        val editor = sharedPreferences.edit()
        editor.putString(PREFS_KEY_USERNAME, username)
        editor.apply()
    }

    fun retrieveUsername(): String {
        val sharedPreferences = MyApplication.ApplicationContext.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        return sharedPreferences.getString(PREFS_KEY_USERNAME, "unknown")
    }
}

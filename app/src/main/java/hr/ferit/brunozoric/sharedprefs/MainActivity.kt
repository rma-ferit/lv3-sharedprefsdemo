package hr.ferit.brunozoric.sharedprefs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.ferit.brunozoric.sharedprefs.preferences.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        usernameSaveAction.setOnClickListener{saveUsername()}
    }

    override fun onResume() {
        super.onResume()
        displayWelcomeMessage()
    }

    private fun displayWelcomeMessage() {
        val username = PreferenceManager().retrieveUsername();
        usernameDisplay.text = getString(R.string.helloMessage, username)
    }

    private fun saveUsername() {
        val username: String = usernameInput.text.toString();
        val preferenceManager = PreferenceManager()
        preferenceManager.saveUsername(username)
        displayWelcomeMessage()
    }


}
